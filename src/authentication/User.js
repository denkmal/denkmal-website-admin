export class User {
    constructor(id, name, token, regionId) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.regionId = regionId;
    }
}