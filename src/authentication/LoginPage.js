import {Login} from 'react-admin';
import React from 'react';

export const LoginPage = () => {
    const background = `https://source.unsplash.com/1600x900/?disco,nightlife,dj,electronic,techno,beer,concert,clubbing`;
    //const background = `https://source.unsplash.com/collection/574331/1600x900`;
    return <Login backgroundImage={background} />
}